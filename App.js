import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import Toast from 'react-native-toast-message';
import {NavigationContainer} from '@react-navigation/native';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import AppNavigation from 'source/navigation';
import theme from 'source/constants/colors';
import {GOOGLE_API_KEY, IOS_CIENT_ID} from 'source/constants/keys';
import store from 'source/redux/store';
import {Provider} from 'react-redux';
import UnAuth from 'source/navigation/UnAuth';
import RcOtpBridge from './source/RcOtpBridge/RcOtpBridge';
import UploadContent from 'source/ui/upload-content';
import CreateNFT from 'source/ui/Create-Nft/CreateNFT';
import CreatePage2 from 'source/ui/Create-Nft/CreatePage2';
import CreateEvent from 'source/ui/Create-Nft/CreateEvent';
import EventScreen from 'source/ui/Create-Nft/EventScreen';
import ArtistProfile from 'source/ui/Create-Nft/ArtistProfile';
import Settings from 'source/ui/Create-Nft/Settings';
import ArtistProfile2 from 'source/ui/Create-Nft/ArtistProfile2';
import Settings2 from 'source/ui/Create-Nft/Settings2';
import Settings3 from 'source/ui/Create-Nft/Settings3';
import Messages from 'source/ui/Create-Nft/Messages';
import ChatScreen from 'source/ui/Create-Nft/ChatScreen';

const App = () => {
  useEffect(() => {
    GoogleSignin.configure({
      webClientId: GOOGLE_API_KEY,
      offlineAccess: false,
      iosClientId: IOS_CIENT_ID,
    });
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar barStyle={'light-content'} backgroundColor={theme.primary} />
        {/* <UnAuth /> */}
        {/* <UploadContent /> */}
        {/* <CreateNFT /> */}
        {/* <CreatePage2 /> */}
        {/* <CreateEvent /> */}
        {/* <EventScreen /> */}
        {/* <ArtistProfile /> */}
        <Settings/>
        {/* <ArtistProfile2 /> */}
        {/* <Settings2/> */}
        {/* <Settings3/> */}
        {/* <Messages/> */}
        {/* <ChatScreen/> */}

        <Toast ref={ref => Toast.setRef(ref)} />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
