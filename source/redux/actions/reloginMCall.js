import {
  MISSED_CALL,
  VERIFY_NUMBER_FAILED,
  VERIFY_NUMBER_REQUESTED,
  VERIFY_NUMBER_SUCCEED,
} from '../ActionTypes';
import endpoint from 'source/constants/endpoint';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Toast from 'react-native-toast-message';

export const reloginMCall = () => dispatch => {
  try {
    dispatch({type: MISSED_CALL, payload: true});
    console.log('action');
    axios.post(endpoint + '/api/v1/check-mobi').then(async response => {
      // if(response.status === 200) {
      //     let token = response.data.phoneNumber;
      //     await AsyncStorage.setItem('@token', token);
      // }else{
      //     console.log("redux/action/reloginMCLL ERROR")
      // }
    });
  } catch (error) {
    console.log('redux/action/reloginMCLL ERROR ' + error.message);
  }
};

export const verifyNumberAction = (number, platform) => dispatch => {
  try {
    dispatch({type: VERIFY_NUMBER_REQUESTED, payload: {number}});

    var data = JSON.stringify({
      number: number,
      type: 'reverse_cli',
      platform: platform,
    });

    var config = {
      method: 'post',
      url: endpoint + '/api/v1/check-mobi',
      headers: {
        Authorization:
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxOThmMGY5ZmEwOWQyMDAxODQ4MThjOCIsImlhdCI6MTYzNzQ4NjU2MywiZXhwIjoxNjQwMDc4NTYzfQ.jwODuGHbUbTnAtCxBm9gC9NUsKnkxgd8K7dVFVNrTu8',
        'Content-Type': 'application/json',
      },
      data: data,
    };
    axios.post(config).then(async response => {
      if (response.status === 200) {
        let token = response.data.token;
        await AsyncStorage.setItem('@token', token);

        dispatch({
          type: VERIFY_NUMBER_SUCCEED,
          payload: {},
        });

        cb && cb();
      } else {
        Toast.show({
          type: 'error',
          position: 'top',
          text1: 'Error',
          text2: response.data.message || 'Something went wrong!',
          visibilityTime: 3000,
          autoHide: true,
          topOffset: 30,
        });

        dispatch({
          type: VERIFY_NUMBER_FAILED,
          payload: false,
        });
      }
      cb && cb();
    });
  } catch (error) {
    console.log('redux/action/reloginMCLL ERROR ' + error.message);
  }
};
