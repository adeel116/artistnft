import { combineReducers } from 'redux';

import auth from './auth';
import utils from './utils';
import reloginMCall from './reloginMCall'

const rootReducer = combineReducers({
    auth,
    utils,
    reloginMCall
})

export default rootReducer;