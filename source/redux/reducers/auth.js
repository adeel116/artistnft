import {
  LOGIN,
  LOGOUT,
  AUTH_LOADING,
  SAVE_REGISTER_DATA,
  VERIFY_NUMBER,
  VERIFY_NUMBER_REQUESTED,
  VERIFY_NUMBER_SUCCEED,
  VERIFY_NUMBER_FAILED,
} from '../ActionTypes';

const initState = {
  registerData: null,
  loading: false,
  isAuth: false,
  user: null,
  missedCallSuccess: false,
  role: '',
};

export default function (state = initState, action) {
  let {payload} = action;
  console.log('authReducer', payload);

  switch (action.type) {
    case AUTH_LOADING:
      return {
        ...state,
        loading: payload,
      };

    case LOGIN:
      return {
        ...state,
        isAuth: true,
        loading: false,
        user: payload.user,
        role: payload.role,
      };

    case LOGOUT:
      return {
        ...state,
        role: '',
        user: null,
        isAuth: false,
        loading: false,
      };

    case SAVE_REGISTER_DATA:
      return {
        ...state,
        registerData: {
          ...state.registerData,
          ...payload,
        },
      };

    case VERIFY_NUMBER_REQUESTED:
      return {
        ...state,
        isAuth: false,
        loading: true,
        user: undefined,
        role: undefined,
        missedCallSuccess: false,
      };

    case VERIFY_NUMBER_SUCCEED:
      return {
        ...state,
        isAuth: true,
        loading: false,
        user: undefined,
        role: undefined,
        missedCallSuccess: true,
      };

    case VERIFY_NUMBER_FAILED:
      return {
        ...state,
        isAuth: true,
        loading: false,
        user: undefined,
        role: undefined,
        missedCallSuccess: false,
      };

    case 'AUTHENTICATE_APP':
      return {
        ...state,
        isAuth: true,
        user: {
          id: '32ejbn2kjen2k3ne90xa8s0x9aZxcVX',
          name: 'Hamza',
        },
      };

    default:
      return {
        ...state,
      };
  }
}
