import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
import CreateEventHeader from './CreateEventHeader'
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import Check from 'source/assets/Check2.png';

function CreateEvent(props) {
  return (
    <ScrollView contentContainerStyle={{height: '135%'}}>
      <View style={[styles.ViewHeight, {backgroundColor: '#383838'}]}>
        <CreateEventHeader />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />
          <Text
            style={{
              fontSize: 15,
              color: '#C6C6C6',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            Live Stream Information
          </Text>
          <Input title="Event Name" style={styles.input} />
          <Input title="Date" style={styles.input} />
          <View style={[styles.flexDirection]}>
            <View style={[styles.rowShadowView, {marginTop: 10}]}>
              <View
                style={[
                  styles.flexDirection,
                  {
                    paddingLeft: 20,
                    paddingRight: 10,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  },
                ]}>
                <Text
                  style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                  Start Time
                </Text>

                <Image source={Check} style={styles.checkImageSize} />
              </View>
            </View>
            <View style={[styles.rowShadowView, {marginTop: 10}]}>
              <View
                style={[
                  styles.flexDirection,
                  {
                    paddingLeft: 20,
                    paddingRight: 10,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  },
                ]}>
                <Text
                  style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                  End Time
                </Text>

                <Image source={Check} style={styles.checkImageSize} />
              </View>
            </View>
          </View>
          <View style={[styles.flexDirection]}>
            <View style={[styles.rowShadowView, {marginTop: 10}]}>
              <View
                style={[
                  styles.flexDirection,
                  {
                    paddingLeft: 20,
                    paddingRight: 10,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  },
                ]}>
                <Text
                  style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                  Youtube
                </Text>

                <Image source={Check} style={styles.checkImageSize} />
              </View>
            </View>
            <View style={[styles.rowShadowView, {marginTop: 10}]}>
              <View
                style={[
                  styles.flexDirection,
                  {
                    paddingLeft: 20,
                    paddingRight: 10,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  },
                ]}>
                <Text
                  style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                  Ticket
                </Text>

                <Image source={Check} style={styles.checkImageSize} />
              </View>
            </View>
          </View>
          <View
            style={[
              styles.flexDirection,
              {
                alignSelf: 'flex-end',

                marginRight: 10,
                marginTop: 10,
                alignItems: 'center',
              },
            ]}>
            <Text
              style={{
                fontSize: 15,
                color: '#C6C6C6',
                fontWeight: 'bold',
                marginRight: 10,
              }}>
              Remind me
            </Text>
            <Switch />
          </View>
          <View
            style={[
              styles.flexDirection,

              {
                width: '90%',
                justifyContent: 'space-between',
                marginTop: 15,
              },
            ]}>
            <Text
              style={{
                fontSize: 15,
                color: '#C6C6C6',
                fontWeight: 'bold',
                marginRight: 10,
              }}>
              Privately Invite Members
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: '#C6C6C6',
                fontWeight: 'bold',
                marginRight: 10,
              }}>
              Invited 02
            </Text>
          </View>
          <Input
            title="Search Artists, Song or Album Name"
            style={styles.input}
          />
          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: '#707070',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: 'white',
                }}></View>
            </View>
            <View style={{flex: 0.6}}>
              <Text style={{fontSize: 17, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD'}}>Fan</Text>
            </View>
            <View style={{flex: 0.1}}></View>
            <Image
              source={Check}
              style={[styles.checkImageSize, {marginTop: 10}]}
            />
          </View>
          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: '#707070',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: 'white',
                }}></View>
            </View>
            <View style={{flex: 0.6}}>
              <Text style={{fontSize: 17, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD'}}>Fan</Text>
            </View>
            <View style={{flex: 0.1}}></View>
            <Image
              source={Check}
              style={[styles.checkImageSize, {marginTop: 10}]}
            />
          </View>
          <TouchableWithoutFeedback
            onPress={() => {
              setModalVisible(true);
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                CREATE BROADCAST
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
}

export default CreateEvent;
