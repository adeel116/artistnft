import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
// import EventHeader from './EventHeader';
import ArtistProfileHeader from './ArtistProfileHeader';
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import userImage from 'source/assets/image.jpeg';
import Check from 'source/assets/Check2.png';

function ArtistProfile(props) {
  return (
    <ScrollView>
      <View style={[styles.eventViewHeight, {backgroundColor: '#383838'}]}>
        <ArtistProfileHeader />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />

          <View
            style={{
              marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
              borderWidth: 2,
              borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize} />
          </View>

          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 20,
              color: 'white',
            }}>
            Bianca Bradley
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',

              color: '#BEBEBE',
            }}>
            Profile
          </Text>
          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 20,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                Artist Youtube Profile
              </Text>

              <Image source={Check} style={[styles.checkImageSize]} />
            </View>
          </View>
          <Text
            style={{
              fontSize: 15,
              color: '#C6C6C6',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            {'Select the Artists NFT to buy from the\n list below'}
          </Text>
          <View
            style={[styles.flexDirection, {justifyContent: 'space-around'}]}>
            <View style={{marginRight: 10}}>
              <View style={styles.ImageBackground2}>
                <Image source={Photo} style={styles.photoIcon2} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: 5,
                  color: 'white',
                }}>
                Dota2
              </Text>
            </View>
            <View>
              <View style={styles.ImageBackground2}>
                <Image source={Photo} style={styles.photoIcon2} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: 5,
                  color: 'white',
                }}>
                Dota2
              </Text>
            </View>
            <View style={{marginLeft: 10}}>
              <View style={styles.ImageBackground2}>
                <Image source={Photo} style={styles.photoIcon2} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: 5,
                  color: 'white',
                }}>
                Dota2
              </Text>
            </View>
          </View>
          <TouchableWithoutFeedback
            onPress={() => {
              setModalVisible(true);
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                BUY
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
}

export default ArtistProfile;
