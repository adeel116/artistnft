import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
  FlatList
} from 'react-native';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
// import EventHeader from './EventHeader';
import ArtistProfileHeader from './ArtistProfileHeader';
import SettingHeader from './SettingHeader';
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import Icon1 from 'source/assets/youtube.png';
import Twitter from 'source/assets/Ticon.png';
import Insta from 'source/assets/insta.png';
import Tiktok from 'source/assets/TikIcon.png';
import Check from 'source/assets/Check2.png';
// import Icon2 from 'source/assets/insta.png';
// import Icon3 from 'source/assets/Ticon.png';
// import Icon4 from 'source/assets/Tikicon.png';
// import Icon5 from 'source/assets/youtube.png';
import userImage from 'source/assets/image.jpeg';

import { Icon } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';

function ArtistProfile(props) {

    const btnData =[
        {
            title:'Twitter',
            // Icon:
        },
        {
          title:'Twitter',
          // Icon:
      },
      {
        title:'Twitter',
        // Icon:
    },
    {
      title:'Twitter',
      // Icon:
  },
  {
    title:'Twitter',
    // Icon:
},
    ]
  return (
    <ScrollView>
      <View style={[styles.eventViewHeight, {backgroundColor: '#383838'}]}>
        <SettingHeader />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          {/* <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          /> */}

<Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 20,
              color: 'white',
              marginBottom:25
            }}>
            Add account details using
          </Text>

          {/* <View
            style={{
              marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
              borderWidth: 2,
              borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize} />
          </View> */}

          {/* <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 20,
              color: 'white',
            }}>
            Kch be
          </Text> */}
          {/* <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',

              color: '#BEBEBE',
            }}>
            Profile
          </Text> */}
          <View>
           {/* <FlatList
                      data={btnData}
                      // numColumns={1}
                      renderItem={({item}) => {
                          return( */}
                            <View style={[styles.ViewShadow, {marginTop: 10}]}>
                            <View
                              style={[
                                styles.flexDirection,
                                {
                                  paddingLeft: 20,
                                  paddingRight: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                              Twitter
                              </Text>
                
                              <View style={{backgroundColor:'white',borderRadius:20,width:35,height:35,alignItems:'center',justifyContent:'center'}}>
                                <Image source={Twitter} style={[styles.checkImageSize]} />
                                </View>
                            </View>
                          </View>

                          <View style={[styles.ViewShadow, {marginTop: 10}]}>
                            <View
                              style={[
                                styles.flexDirection,
                                {
                                  paddingLeft: 20,
                                  paddingRight: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                              Instagram
                              </Text>
                
                              <View style={{backgroundColor:'white',borderRadius:20,width:35,height:35,alignItems:'center',justifyContent:'center'}}>
                                <Image source={Insta} style={[styles.checkImageSize]} />
                                </View>
                            </View>
                          </View>

                          <View style={[styles.ViewShadow, {marginTop: 10}]}>
                            <View
                              style={[
                                styles.flexDirection,
                                {
                                  paddingLeft: 20,
                                  paddingRight: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                              Youtube
                              </Text>
                
                              <View style={{backgroundColor:'white',borderRadius:20,width:35,height:35,alignItems:'center',justifyContent:'center'}}>
                                <Image source={Icon1} style={[styles.checkImageSize]} />
                                </View>
                            </View>
                          </View>

                          <View style={[styles.ViewShadow, {marginTop: 10}]}>
                            <View
                              style={[
                                styles.flexDirection,
                                {
                                  paddingLeft: 20,
                                  paddingRight: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                              TikTok
                              </Text>
                
                              <View style={{backgroundColor:'white',borderRadius:20,width:35,height:35,alignItems:'center',justifyContent:'center'}}>
                                <Image source={Tiktok} style={[styles.checkImageSize]} />
                                </View>
                            </View>
                          </View>

                          <View style={[styles.ViewShadow, {marginTop: 10}]}>
                            <View
                              style={[
                                styles.flexDirection,
                                {
                                  paddingLeft: 20,
                                  paddingRight: 20,
                                  flex: 1,
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                              Twitch
                              </Text>
                
                              <View style={{backgroundColor:'white',borderRadius:20,width:35,height:35,alignItems:'center',justifyContent:'center'}}>
                                <Image source={Check} style={[styles.checkImageSize]} />
                                </View>
                            </View>
                          </View>
                                                 {/* );
                                                }}
                                              /> */}
                                              </View>
                          
                      

          
          <View style={{marginTop:50,marginBottom:15}}>
          <TouchableWithoutFeedback
            onPress={() => {

            }}>
            <View style={styles.NextBtn2}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              ADD ACOUNTS TO ARTIST PROFILE
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback
            onPress={() => {

            }}>
            <View style={[styles.NextBt3,{marginTop:20}]}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              ADD ACOUNTS TO HOOTSUITE
              </Text>
            </View>
          </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

export default ArtistProfile;
