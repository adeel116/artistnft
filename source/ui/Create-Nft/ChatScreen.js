import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Switch,
    ScrollView,
    TouchableWithoutFeedback,
    Modal,
    TextInput
  } from 'react-native';
import theme from 'source/constants/colors';
import BACK from 'source/assets/back.png';
import VERTICAL_MENU from 'source/assets/vertical-menu.png';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
// import EventHeader from './EventHeader';
import ArtistProfileHeader from './ArtistProfileHeader';
import APP_NAME from 'source/assets/app_name.png';
import Photo from 'source/assets/photo.png';
import userImage from 'source/assets/image.jpeg';
import Icon1 from 'source/assets/pagePin.png';
import Icon2 from 'source/assets/micIcon1.png';
import Icon3 from 'source/assets/sendIcon.png';
import Tiktok from 'source/assets/TikIcon.png';
import Check from 'source/assets/Check2.png';

function ChatScreen(props) {
  return (
      <View style={{flex:1,backgroundColor:theme.primary}}>
    <View style={styless.container}>
      <Image source={BACK} style={styless.image} />
      
      <Image source={VERTICAL_MENU} style={styless.image} />
    </View>
    <View style={{flex:1}} >
        <View style={{alignItems:'center',backgroundColor:'rgba(98, 98, 98, 0.3)',width:'100%',paddingBottom:20}}>
    
    <View
            style={{
            //   marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
              borderWidth: 2,
              borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize} />
          </View>

          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 20,
              color: 'white',
            }}>
            Bianca Bradley
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',

              color: '#BEBEBE',
            }}>
            Typing...
          </Text>
          </View>
          <ScrollView>
          <View style={{padding:10,}}>
          <View style={{flexDirection:'row',marginTop:20}}>
          <View
            style={{
            //   marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
            //   borderWidth: 2,
            //   borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize1} />
          </View>
          <View>
          <View style={{backgroundColor:'#EA4836',borderRadius:25,padding:15,justifyContent:'center'}}>
              <Text style={{fontSize:13,color:'white'}}>Hi, Affan</Text>
          </View>
          <Text style={{fontSize:12,color:'#BEBEBE',marginLeft:10,marginTop:5}}>2:39 PM</Text>
          </View>
          </View>

          <View style={{flexDirection:'row',marginTop:20}}>
          <View
            style={{
            //   marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
            //   borderWidth: 2,
            //   borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize1} />
          </View>
          <View>
          <View style={{backgroundColor:'#EA4836',borderRadius:25,padding:15,justifyContent:'center'}}>
              <Text style={{fontSize:13,color:'white'}}>
                  {'What are you doing?? I am waiting\n for you…..'}
              </Text>
          </View>
          <Text style={{fontSize:12,color:'#BEBEBE',marginLeft:10,marginTop:5}}>2:39 PM</Text>
          </View>
          </View>

          <View style={{flexDirection:'row',marginTop:20,alignSelf:'flex-end'}}>
          <View
            style={{
            //   marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
            //   borderWidth: 2,
            //   borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize1} />
          </View>
          <View>
          <View style={{backgroundColor:'#EA4836',borderRadius:25,padding:15,justifyContent:'center'}}>
              <Text style={{fontSize:13,color:'white'}}>
              Yes, i am cmng
              </Text>
          </View>
          <Text style={{fontSize:12,color:'#BEBEBE',marginLeft:10,marginTop:5}}>2:39 PM</Text>
          </View>
          </View>
          
          

          </View>
          </ScrollView>

          <View style={{height:50,width:350,backgroundColor:'white',borderRadius:30,marginTop:60,alignSelf:'center',flexDirection:'row',alignItems:'center',marginBottom:20}}>
          <TextInput style={{flex:0.7,marginLeft:20,}} 
            placeholder="Type Message"/>
            <View style={{flexDirection:'row',justifyContent: 'space-between',flex:0.3,marginRight:10}}>
            <Image source={Icon1} style={styles.imageSize2} />
            <Image source={Icon2} style={styles.imageSize2} />
            <Image source={Icon3} style={styles.imageSize2} />
            </View>
          </View>
          
    </View>
    </View>
  );
}
const styless = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(98, 98, 98, 0.3)',
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontFamily: 'Sansation',
    fontSize: 16,
  },
});

export default ChatScreen;
