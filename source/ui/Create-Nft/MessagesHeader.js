import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import theme from 'source/constants/colors';
import BACK from 'source/assets/back.png';
import VERTICAL_MENU from 'source/assets/vertical-menu.png';

function ArtistProfileHeader(props) {
  return (
    <View style={styles.container}>
      <Image source={BACK} style={styles.image} />

      <Text style={styles.headerText}>Messages</Text>

      <Image source={VERTICAL_MENU} style={styles.image} />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.primary,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    color: 'white',
    fontFamily: 'Sansation',
    fontSize: 16,
  },
});

export default ArtistProfileHeader;
