import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../upload-content/styles';
import CreateNftHeadar from './CreateNftHeadar';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';

function CreateNFT(props) {
  return (
    <ScrollView >
      <View style={[styles.scrollViewContainer, {backgroundColor: '#383838'}]}>
        <CreateNftHeadar />
        <View style={styles.container}>
          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />
          <View
            style={{
              width: 122,
              height: 122,
              backgroundColor: '#474747',
              marginTop: 40,
              borderRadius: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              source={Photo}
              style={{height: 90, width: 90, resizeMode: 'contain'}}
            />
          </View>
          <Text style={[styles.heading1, styles.transparentContainer]}>
            {
              'Add special traits and other attributes to\n increase the uniqueness of your NFT'
            }
          </Text>
          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
                Acces rights for NFT
              </Text>

              <Switch />
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
                {'Create 4 digit Password to view or\n listen to the NFT'}
              </Text>

              <Switch />
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
                {'Share NFT for Listening/Viewing\n with up to 5 more Holders'}
              </Text>

              <Switch />
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                {
                  'Restrict Sharing of the above Holders\n to an additional 5 more Holders'
                }
              </Text>

              <Switch />
            </View>
          </View>
          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                Promo Link
              </Text>

              <Switch />
            </View>
          </View>
          <TouchableWithoutFeedback
            onPress={() => {
              console.log('NEXT');
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                NEXT
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
}
// const styles = StyleSheet.create({});

export default CreateNFT;
