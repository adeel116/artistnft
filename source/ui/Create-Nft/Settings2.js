import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../upload-content/styles';
import CreateNftHeadar from './CreateNftHeadar';
import SettingHeader from './SettingHeader';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import close from 'source/assets/close.png';
import APP_NAME from 'source/assets/app_name.png';

function Settings2(props) {
  return (
    <ScrollView>
      <View style={[styles.scrollViewContainer, {backgroundColor: '#383838'}]}>
        <SettingHeader />
        
        <View style={styles.container}>
        <Image source={APP_NAME} style={styles.appName} />
          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />
          <Text style={[styles.heading1, styles.transparentContainer,{marginBottom:10}]}>
            {
              'Assign and Manage Your Royalties'
            }
          </Text>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                // borderBottomWidth: 1,
                // borderColor: '#707070',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 5,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={Photo}
              style={[styles.checkImageSize1]}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>#1 Example Track 01</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',marginTop:5}}>My track splits : 50%</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>EDIT SPLIT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 5,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={Photo}
              style={[styles.checkImageSize1]}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>#1 Example Track 02</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',marginTop:5}}>My track splits : 50%</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>EDIT SPLIT</Text>
            </View>
            </View>
          </View>


          <View
            style={{
              width: 340,
              backgroundColor: '#474747',
              marginTop: 20,
              borderRadius: 10,
              padding:10
            }}>
                <View style={{flexDirection:'row',justifyContent: 'space-between',marginBottom:10}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'white'}}>#1 Example Track 03</Text>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Text style={{fontSize:14,fontWeight:'bold',color:'white',marginRight:5}}>Cancel</Text>
                    <View style={{width:15,height:15,borderRadius:10,backgroundColor:'#EA4836',justifyContent:'center',alignItems:'center'}}>
                    <Image
              source={close}
              style={[styles.checkImageSize2]}
            />
                    </View>
                    </View>
                </View>

                <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 5,
                  backgroundColor: '#383838',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Mark Delta (me)</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',marginTop:8}}>My track splits : 50%</Text>
            </View>
          </View>
          <View style={{marginTop:20,marginBottom:15}}>

          <Input
            title="Name"
            style={{marginTop:10,width:"100%"}}
          />

<Input
            title="Email"
            style={{marginTop:10,width:"100%"}}
          />
          <Input
            title="Split %"
            style={{marginTop:10,width:"100%"}}
          />
          </View>
           
          </View>
          <View
            style={[
              styles.flexDirection,
              {
                height: 50,
                width: '87%',
                //   justifyContent: 'center',
                alignItems: 'center',
                marginTop: 5,
              },
            ]}>
            <View style={{width: '75%'}}>
              <Text style={{fontSize: 12, color: '#C6C6C6', fontWeight: '700'}}>
                {
                  'ADD/Remove MORE FIELDS UPTO 10 legal\n Names, Telephones & Percentages'
                }
              </Text>
            </View>
            <View style={[styles.flexDirection, {marginLeft: 20}]}>
              <View
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  backgroundColor: '#615959',
                  // justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>-</Text>
              </View>
              <View
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  backgroundColor: 'white',
                  marginLeft: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>+</Text>
              </View>
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
                {'Songwriter (Royalties on the\n composition copyright)'}
              </Text>

              <Switch />
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
                {'Performer (Royalties on the\n recording copyright)'}
              </Text>

              <Switch />
            </View>
          </View>

          

          
         
          <TouchableWithoutFeedback
            onPress={() => {
              console.log('NEXT');
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              SAVE EDITS
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
}
// const styles = StyleSheet.create({});

export default Settings2;
