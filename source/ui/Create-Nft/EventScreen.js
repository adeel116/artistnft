import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
import EventHeader from './EventHeader';
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import YoutubeIcon from 'source/assets/youtube.png';
import Check from 'source/assets/Check2.png';

function EventScreen(props) {
  return (
    <ScrollView contentContainerStyle={{height: '120%'}}>
      <View style={[styles.eventViewHeight, {backgroundColor: '#383838'}]}>
        <EventHeader />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />
          <Text
            style={{
              fontSize: 15,
              color: '#C6C6C6',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            Live Stream Information
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: 'white',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            {'1. https://www.youtube.com/watch?v=SG5fx\n91Ep9U'}
          </Text>
          <View style={[styles.EventView, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 30,
                  paddingRight: 30,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 35,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image source={YoutubeIcon} style={styles.youtubeIcon} />
              </View>
              <View style={{marginLeft: 10}}>
                <Text
                  style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                  Pop Rock Live Stream | 12-04-2021
                </Text>
                <Text
                  style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                  9AM - 11AM
                </Text>
              </View>
            </View>
            <TouchableWithoutFeedback
              onPress={() => {
                setModalVisible(true);
              }}>
              <View style={styles.TicketBtn}>
                <Text
                  style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                  CREATE BROADCAST
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <Text
            style={{
              fontSize: 15,
              color: 'white',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            {'1. https://www.youtube.com/watch?v=SG5fx\n91Ep9U'}
          </Text>
          <View style={[styles.EventView, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 30,
                  paddingRight: 30,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 35,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image source={YoutubeIcon} style={styles.youtubeIcon} />
              </View>
              <View style={{marginLeft: 10}}>
                <Text
                  style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                  Pop Rock Live Stream | 12-04-2021
                </Text>
                <Text
                  style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>
                  9AM - 11AM
                </Text>
              </View>
            </View>
            <TouchableWithoutFeedback
              onPress={() => {
                setModalVisible(true);
              }}>
              <View style={styles.TicketBtn}>
                <Text
                  style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                  CREATE BROADCAST
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

export default EventScreen;
