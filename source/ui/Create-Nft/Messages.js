import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
  FlatList
} from 'react-native';
import styles from '../upload-content/styles';
import CreateNftHeadar from './CreateNftHeadar';
import MessagesHeader from './MessagesHeader'
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import User from 'source/assets/image.jpeg';

function Messages(props) {

    const messageList =[
        {
            image:User,
            Name:'Bianca Bradley'
        },
        {
            image:User,
            Name:'Bianca Gibson'
        },
        {
            image:User,
            Name:'Bianca Fisher'
        },
        {
            image:User,
            Name:'Bianca Bradley'
        },
        {
            image:User,
            Name:'Bianca Hawkins'
        },
        {
            image:User,
            Name:'Bianca Ford'
        },
        {
            image:User,
            Name:'Bianca Bradley'
        },
    ]
  return (
    <ScrollView contentContainerStyle={{height: '128%'}}>
      <View style={[styles.ViewHeight, {backgroundColor: '#383838'}]}>
        <MessagesHeader />
        <View style={styles.container}>
          {/* <Image source={APP_NAME} style={styles.appName} /> */}

          <Input
            title="Search Artists, Song or Album Name"
            style={[styles.inputSeach,{marginBottom:10}]}
          />
          {/* <View style={{flex:1}}>
           <FlatList
                                        data={messageList}
                                        renderItem={({item}) => {
                                          return ( */}
                                          
             <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          <View
            style={[
              {
                marginRight: 5,
                marginLeft: 5,
                marginTop: 10,
                borderBottomWidth: 1,
                borderColor: 'rgba(112, 112, 112, 0.3)',
                paddingBottom: 10,
              },
              styles.flexDirection,
            ]}>
            <View style={{flex: 0.2}}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#474747',
                  alignItems:'center',
                  justifyContent:'center'
                }}>
                    <Image
              source={User}
              style={{width:50,height:50,borderRadius:25}}
            />
                </View>
            </View>
            <View style={{flex: 0.5,}}>
              <Text style={{fontSize: 14, color: 'white'}}>Bianca Bradley</Text>
              <Text style={{fontSize: 12, color: '#ADADAD',}}>Artist</Text>
            </View>
            <View style={{flex: 0.2,justifyContent:'center'}}>
            <View style={{height:25,borderRadius:5,backgroundColor:"#EA4836",justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize: 10, color: 'white'}}>SELECT</Text>
            </View>
            </View>
          </View>

          
            {/* );
        }}
      />
      </View> */}
         
        </View>
      </View>
    </ScrollView>
  );
}

export default Messages;
