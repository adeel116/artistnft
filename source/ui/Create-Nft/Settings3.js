import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../upload-content/styles';
import CreateNftHeadar from './CreateNftHeadar';
import SettingHeader from './SettingHeader';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import close from 'source/assets/close.png';
import APP_NAME from 'source/assets/app_name.png';

function Settings2(props) {
  return (
    <ScrollView>
      <View style={[styles.scrollViewContainer, {backgroundColor: '#383838'}]}>
        <SettingHeader />
        <View style={styles.container}>
        <Image source={APP_NAME} style={styles.appName} />
          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />
          <Text style={[styles.heading1, styles.transparentContainer,{marginBottom:10}]}>
            {
              'Rights Assignment to Content Type'
            }
          </Text>
          <View style={{flexDirection:'row',marginTop:20}}>
              <View style={[styles.boxView,{marginRight:5}]}>
                  <View style={styles.roundView}></View>
                  <Text style={{fontSize:12,color:'white',marginTop:10}}>Recorded</Text>
              </View>
              <View style={[styles.boxView,{marginLeft:5}]}>
              <View style={styles.roundView}></View>
                  <Text style={{fontSize:12,color:'white',marginTop:10}}>Recorded Streaming</Text>
              </View>
          </View>
          <View style={{flexDirection:'row',marginTop:10}}>
          <View style={[styles.boxView,{marginRight:5}]}>
          <View style={styles.roundView}></View>
                  <Text style={{fontSize:12,color:'white',marginTop:10}}>Live Streaming</Text>
          </View>
              <View style={[styles.boxView,{marginLeft:5}]}>
              <View style={styles.roundView}></View>
                  <Text style={{fontSize:12,color:'white',marginTop:10}}>NFT</Text>
              </View>
          </View>

          

         


          <View style={[styles.ViewShadow, {marginTop: 30}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              Recorded
              </Text>

              <Switch />
            </View>
          </View>

          

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              Recorded Streaming
              </Text>

              <Switch />
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              Live Streaming
              </Text>

              <Switch />
            </View>
          </View>
          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              NFT
              </Text>

              <Switch />
            </View>
          </View>
          <View style={[styles.ViewShadow, {marginTop: 10,marginBottom:20}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 10,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 15, fontWeight: 'bold', color: 'white'}}>
              Assignment of Rights
              </Text>

              <Switch />
            </View>
          </View>

          

          
         
          {/* <TouchableWithoutFeedback
            onPress={() => {
              console.log('NEXT');
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              SAVE EDITS
              </Text>
            </View>
          </TouchableWithoutFeedback> */}
        </View>
      </View>
    </ScrollView>
  );
}
// const styles = StyleSheet.create({});

export default Settings2;
