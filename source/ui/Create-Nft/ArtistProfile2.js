import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import styles from '../upload-content/styles';
// import CreateNftHeadar from './CreateNftHeadar';
// import EventHeader from './EventHeader';
import ArtistProfileHeader from './ArtistProfileHeader';
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import userImage from 'source/assets/image.jpeg';
import Icon1 from 'source/assets/youtube.png';
import Twitter from 'source/assets/Ticon.png';
import Insta from 'source/assets/insta.png';
import Tiktok from 'source/assets/TikIcon.png';
import Check from 'source/assets/Check2.png';


function ArtistProfile2(props) {

    const Icons = [
        {
            icon:Icon1
        },
        {
            icon:Twitter
        },
        {
            icon:Insta
        },
        {
            icon:Tiktok
        },
        {
            icon:Check
        },
    ]
  return (
    <ScrollView>
      <View style={[styles.eventViewHeight, {backgroundColor: '#383838'}]}>
        <ArtistProfileHeader />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />

          <View
            style={{
              marginTop: 40,
              width: 65,
              height: 65,
              borderRadius: 35,
              borderWidth: 2,
              borderColor: '#EA4836',
            }}>
            <Image source={userImage} style={styles.imageSize} />
          </View>

          <Text
            style={{
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 20,
              color: 'white',
            }}>
            Bianca Bradley
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',

              color: '#BEBEBE',
            }}>
            Profile
          </Text>
          

          <View style={[styles.ImageBackground3,styles.justifyContentSpace,{flexDirection:'row'}]}>
              <View style={styles.artWorksBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              Artworks
              </Text>
              </View>
              <View style={styles.artWorksBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              About
              </Text>
              </View>
          </View>
          
          <View
            style={[styles.flexDirection, {justifyContent: 'space-around',marginBottom:50}]}>
            <View style={{marginRight: 15}}>
              <View style={styles.ImageBackground4}>
                <Image source={Photo} style={styles.photoIcon3} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: 5,
                  color: '#C6C6C6',
                }}>
                Dota2
              </Text>
            </View>
            <View style={{marginLeft:5}}>
              <View style={styles.ImageBackground4}>
                <Image source={Photo} style={styles.photoIcon3} />
              </View>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  marginTop: 5,
                  color: '#C6C6C6',
                }}>
                PlayGwent
              </Text>
            </View>
          </View>
       <View style={{width:375,height:120,backgroundColor:'#474747',marginTop:30,alignItems:'center'}}>
       <Text style={styles.heading3}>
              {'Change Your Social Networks'}
            </Text>
            <View style={{flexDirection:'row',marginTop:15}}>
            {Icons.map(item => (
            <View style={{width:49,height:49,borderRadius:25,backgroundColor:'white',alignItems:'center',justifyContent:'center',marginRight:5,marginLeft:5}}>
            <Image source={item.icon} style={[styles.checkImageSize1]} />
            </View>
            ))}
            {/* <View style={{width:49,height:49,borderRadius:25,backgroundColor:'white',alignItems:'center',justifyContent:'center'}}>
            <Image source={Tiktok} style={[styles.checkImageSize1]} />
            </View> */}
            </View>
       </View>
        </View>
      </View>
    </ScrollView>
  );
}

export default ArtistProfile2;
