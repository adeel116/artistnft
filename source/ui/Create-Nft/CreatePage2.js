import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Switch,
  ScrollView,
  TouchableWithoutFeedback,
  Modal,
} from 'react-native';
import styles from '../upload-content/styles';
import CreateNftHeadar from './CreateNftHeadar';
import APP_NAME from 'source/assets/app_name.png';
import Input from 'source/common/input';
import Photo from 'source/assets/photo.png';
import Check from 'source/assets/Check2.png';

function CreatePage2(props) {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <ScrollView>
      <View style={[styles.scrollViewContainer, {backgroundColor: 'red'}]}>
        <Modal
          style={styles.modalView}
          animationType="fade"
          transparent={true}
          visible={modalVisible}>
          <View
            style={{
              height: 250,
              backgroundColor: 'white',
              marginTop: 220,
              marginLeft: 20,
              marginRight: 20,
              borderRadius: 10,
              padding: 20,
            }}>
            <TouchableWithoutFeedback
              onPress={() => {
                setModalVisible(false);
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  borderRadius: 4,
                  borderWidth: 2,
                  borderColor: 'black',
                  alignSelf: 'flex-end',
                  //   justifyContent: 'center',
                  //   alignItems: 'center',
                }}>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontWeight: 'bold',
                    marginTop: -1.5,
                  }}>
                  X
                </Text>
              </View>
            </TouchableWithoutFeedback>
            <View style={{alignItems: 'center', marginTop: 40}}>
              <Image source={Check} style={styles.uploadIcon} />

              <Text style={styles.modalText}>
                {'Your file has been added to\n your “Media Folder”.'}
              </Text>
            </View>
          </View>
        </Modal>
        <CreateNftHeadar />
        <View style={styles.container}>
          <Image source={APP_NAME} style={styles.appName} />

          <Input
            title="Search Artists, Song or Album Name"
            style={styles.inputSeach}
          />

          <Text
            style={{
              fontSize: 15,
              color: '#C6C6C6',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            Buy it Now
          </Text>
          <Input title="105.5$" style={styles.input} />
          <Text
            style={{
              fontSize: 15,
              color: '#C6C6C6',
              fontWeight: 'bold',
              width: 315,
              marginTop: 25,
            }}>
            Assign Royalities
          </Text>
          <Input title="Name" style={styles.input} />
          <Input title="Telephone" style={styles.input} />
          <Input title="Split %" style={styles.input} />

          <View
            style={[
              styles.flexDirection,
              {
                height: 50,
                width: '87%',
                //   justifyContent: 'center',
                alignItems: 'center',
                marginTop: 5,
              },
            ]}>
            <View style={{width: '75%'}}>
              <Text style={{fontSize: 12, color: '#C6C6C6', fontWeight: '700'}}>
                {
                  'ADD/Remove MORE FIELDS UPTO 10 legal\n Names, Telephones & Percentages'
                }
              </Text>
            </View>
            <View style={[styles.flexDirection, {marginLeft: 20}]}>
              <View
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  backgroundColor: '#615959',
                  // justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>--</Text>
              </View>
              <View
                style={{
                  height: 20,
                  width: 20,
                  borderRadius: 10,
                  backgroundColor: 'white',
                  marginLeft: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 14, fontWeight: 'bold'}}>+</Text>
              </View>
            </View>
          </View>

          <View style={[styles.ViewShadow, {marginTop: 10}]}>
            <View
              style={[
                styles.flexDirection,
                {
                  paddingLeft: 20,
                  paddingRight: 20,
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                },
              ]}>
              <Text style={{fontSize: 14, fontWeight: 'bold', color: 'white'}}>
                Stablecoin (USDC $) - Default
              </Text>

              <Image source={Check} style={styles.checkImageSize} />
            </View>
          </View>
          <TouchableWithoutFeedback
            onPress={() => {
              setModalVisible(true);
            }}>
            <View style={styles.NextBtn}>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                CREATE NFT
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </ScrollView>
  );
}

export default CreatePage2;
