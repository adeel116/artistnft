import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  StyleSheet,
  Modal,
} from 'react-native';
import styles from './styles';

import DESKTOP from 'source/assets/Desktop.png';
import CLOUD from 'source/assets/cloud-upload.png';
import DROP_BOX from 'source/assets/DropBox.png';
import Photo from 'source/assets/photo.png';
import Check from 'source/assets/Check2.png';
import Gif from 'source/assets/gif.png';
import Mic from 'source/assets/mic.png';
import GOOGLE_DRIVE from 'source/assets/GDrive.png';
import APP_NAME from 'source/assets/app_name.png';
import CloseBtn from 'source/assets/close1.png';

import OutlinedButton from 'source/common/button/OutlinedButton';
import LanguageSelector from 'source/common/languageSelector';
import {getRoles} from 'source/redux/actions/utils';
import {useDispatch} from 'react-redux';
import HeaderComponent from './components/HeaderComponent';
import Input from 'source/common/input';
import {Button} from 'react-native-paper';
import {
  GDrive,
  MimeTypes,
} from '@robinbobin/react-native-google-drive-api-wrapper';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {pickFile} from 'source/helpers/documentHelper';

export default function UploadContent({navigation}) {
  const dispatch = useDispatch();
  const [toggler, setToggler] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);

  const gdrive = new GDrive();

  const test = async () => {
    var tokens = await GoogleSignin.getTokens();
    console.log(tokens);
    gdrive.per;
    gdrive.accessToken = gdrive.accessToken = tokens.accessToken;
    var binary = await gdrive.files.getBinary(fileId, null, '1-1');
    console.log(binary);
    // const id = (
    //   await gdrive.files
    //     .newMultipartUploader()
    //     .setData([1, 2, 3, 4, 5], MimeTypes.BINARY)
    //     .setRequestBody({
    //       name: 'multipart_bin',
    //     })
    //     .execute()
    // ).id;
  };

  useEffect(() => {
    //test();
  }, []);

  const pickLocalFiles = () => {
    pickFile();
  };

  return (
    <View>
      <Modal
        style={styles.modalView}
        animationType="fade"
        transparent={true}
        visible={modalVisible}>
        <View
          style={{
            height: 250,
            backgroundColor: 'white',
            marginTop: 220,
            marginLeft: 20,
            marginRight: 20,
            borderRadius: 10,
            padding: 20,
          }}>
          <TouchableWithoutFeedback
            onPress={() => {
              setModalVisible(false);
            }}>
            <View
              style={{
                width: 20,
                height: 20,
                borderRadius: 4,
                borderWidth: 2,
                borderColor: 'black',
                alignSelf: 'flex-end',
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  fontWeight: 'bold',
                  marginTop: -1.5,
                }}>
                X
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <View style={{alignItems: 'center', marginTop: 40}}>
            <Image source={Check} style={styles.uploadIcon} />

            <Text style={styles.modalText}>
              {'Your file has been added to\n your “Media Folder”.'}
            </Text>
          </View>
        </View>
      </Modal>
      <HeaderComponent />
      <ScrollView contentContainerStyle={styles.scrollViewContainer2}>
        <TouchableWithoutFeedback onPress={() => setToggler(Math.random())}>
          <View style={styles.container}>
            <Image source={APP_NAME} style={styles.appName} />

            <Input
              title="Search Artists, Song or Album Name"
              style={styles.inputSeach}
            />
            <Text style={styles.heading1}>
              {'Please upload your Content file to\n begin the NFT process'}
            </Text>

            <View style={styles.uploadIconsView}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  width: '100%',
                }}>
                <TouchableWithoutFeedback onPress={pickLocalFiles}>
                  <Image source={DESKTOP} style={styles.uploadIcon} />
                </TouchableWithoutFeedback>

                <Image source={GOOGLE_DRIVE} style={styles.uploadIcon} />

                <Image source={DROP_BOX} style={styles.uploadIcon} />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  width: '100%',
                  marginTop: 20,
                }}>
                <Image source={CLOUD} style={styles.uploadIcon} />
              </View>
            </View>

            <View
              style={[
                styles.flexDirection,
                styles.marginTop,
                {
                  width: '80%',
                  borderBottomWidth: 1,
                  borderColor: 'white',
                  paddingBottom: 15,
                },
              ]}>
              <View style={styles.ImageBackground}>
                <Image source={Photo} style={styles.photoIcon} />
              </View>

              <View style={[styles.flexDirection, styles.justifyContentSpace]}>
                <View style={[{marginLeft: 15}]}>
                  <View
                    style={[styles.flexDirection, styles.justifyContentSpace]}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Hip Hop Cover.png
                    </Text>
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: 'white',
                        marginLeft: 10,
                      }}>
                      7.6 MB
                    </Text>
                    <Image source={CloseBtn} style={styles.closeIcon} />
                  </View>
                  <Text style={{color: 'white'}}>Completed</Text>
                </View>
                {/* <Image source={CloseBtn} style={styles.closeIcon} /> */}
              </View>
            </View>

            <View
              style={[
                styles.flexDirection,
                styles.marginTop,
                {
                  width: '80%',
                  borderBottomWidth: 1,
                  borderColor: 'white',
                  paddingBottom: 15,
                },
              ]}>
              <View style={styles.ImageBackground}>
                <Image source={Gif} style={styles.photoIcon} />
              </View>

              <View style={[styles.flexDirection, styles.justifyContentSpace]}>
                <View style={[{marginLeft: 15}]}>
                  <View
                    style={[styles.flexDirection, styles.justifyContentSpace]}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Hip Hop Cover.png
                    </Text>
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: 'white',
                        marginLeft: 10,
                      }}>
                      7.6 MB
                    </Text>
                    <Image source={CloseBtn} style={styles.closeIcon} />
                  </View>
                  <Text style={{color: 'white'}}>Completed</Text>
                </View>
                {/* <Image source={CloseBtn} style={styles.closeIcon} /> */}
              </View>
            </View>

            <View
              style={[
                styles.flexDirection,
                styles.marginTop,
                {
                  width: '80%',
                  borderBottomWidth: 1,
                  borderColor: 'white',
                  paddingBottom: 15,
                },
              ]}>
              <View style={styles.ImageBackground}>
                <Image source={Mic} style={styles.photoIcon} />
              </View>

              <View style={[styles.flexDirection, styles.justifyContentSpace]}>
                <View style={[{marginLeft: 15}]}>
                  <View
                    style={[styles.flexDirection, styles.justifyContentSpace]}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Hip Hop Cover.png
                    </Text>
                    <Text
                      style={{
                        alignSelf: 'center',
                        fontSize: 12,
                        color: 'white',
                        marginLeft: 10,
                      }}>
                      7.6 MB
                    </Text>
                    <Image source={CloseBtn} style={styles.closeIcon} />
                  </View>
                  <Text style={{color: 'white'}}>Completed</Text>
                </View>
                {/* <Image source={CloseBtn} style={styles.closeIcon} /> */}
              </View>
            </View>

            <TouchableWithoutFeedback
              onPress={() => {
                setModalVisible(true);
              }}>
              <View style={styles.ApproveBtn}>
                <Text
                  style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                  APPROVE
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
}
