import React from 'react'
import { View, Image, Text } from 'react-native'
import styles from './styles'

import LOGO from 'source/assets/logo.png';
import APP_NAME from 'source/assets/app_name.png';
import PROFILE from 'source/assets/profile.png';
import NFT from 'source/assets/nft.png';
import PROMOTION from 'source/assets/promotion.png';
import ROYALITY from 'source/assets/royality.png';
import WALLET from 'source/assets/wallet.png';
import MESSAGE from 'source/assets/message.png';
import LOGOUT from 'source/assets/logout.png';
import NavItem from './NavItem';
import { ScrollView } from 'react-native-gesture-handler';

export default function Dashboard({ navigation }) {
    return (
        <ScrollView contentContainerStyle={styles.contentContainer}>
            <View style={styles.container}>
                <Image
                    source={LOGO}
                    style={styles.logo}
                />
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />

                <View style={{ width: '100%' }}>
                    <NavItem
                        icon={PROFILE}
                        title="My Profile"
                        onPress={()=>navigation.navigate('Profile')}
                    />
                    <NavItem
                        icon={NFT}
                        title="NFT"
                    />
                    <NavItem
                        icon={PROMOTION}
                        title="Promotion"
                    />
                    <NavItem
                        icon={ROYALITY}
                        title={`Royalities &\nPublishing`}
                    />
                    <NavItem
                        icon={WALLET}
                        title="My Wallet"
                    />
                    <NavItem
                        icon={MESSAGE}
                        title="Messages"
                    />
                    <NavItem
                        icon={LOGOUT}
                        title="Logout"
                    />
                </View>
            </View>
        </ScrollView>
    )
}
