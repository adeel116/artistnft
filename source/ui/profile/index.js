import React, { useState } from 'react';
import { View, Image, Switch, Text, ScrollView } from 'react-native';
import styles from './styles';
import theme from 'source/constants/colors';
import ToggleSwitch from 'toggle-switch-react-native'

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import QR from 'source/assets/qr.png';
import MORE from 'source/assets/more.png';
import SocialNetwork from './SocialNetwork';

export default function Selection() {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    return (
        <ScrollView contentContainerStyle={styles.scrollview}>
            <View style={styles.container}>
                
                <View style={styles.row}>
                    <Image
                        source={APP_NAME}
                        style={styles.appName}
                    />
                </View>
                
                <View style={[styles.row, styles.pading]}>
                    <Text style={[styles.heading2, styles.heading3]}>Notifications</Text>
                    <ToggleSwitch
                        onColor={theme.secondary}
                        offColor="#767577"
                        ios_backgroundColor="#3e3e3e"
                        onToggle={toggleSwitch}
                        isOn={isEnabled}
                    />
                </View>

                <Text style={[styles.heading1, { marginTop: 35 }]}>Profile QR CODE</Text>
                
                <Image
                    source={QR}
                    style={styles.otpImage}
                />


                <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                    <SocialNetwork />

                    <Button
                        title="MAIN MENU"
                        onPress={()=>alert('Yes')}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}