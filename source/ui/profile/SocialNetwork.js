import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './styles'

import SOUND_CLOUD from 'source/assets/soundcloud.png'
import YOUTUBE from 'source/assets/youtube.png'
import BANDCAMP from 'source/assets/bandcamp.png'
import MERCH from 'source/assets/merch.png'

export default function SocialNetwork() {
    return (
        <View>
            <Text style={[styles.heading2, { fontSize: 15 }]}>
                Change Your Social Networks
            </Text>

            <View style={styles.networksView}>
                <View style={styles.iconView}>
                    <Image
                        style={styles.icon}
                        source={SOUND_CLOUD}
                    />
                </View>
                <View style={styles.iconView}>
                    <Image
                        style={styles.icon}
                        source={BANDCAMP}
                    />
                </View>
                <View style={styles.iconView}>
                    <Image
                        style={styles.icon}
                        source={YOUTUBE}
                    />
                </View>
                <View style={styles.iconView}>
                    <Image
                        style={styles.icon}
                        source={MERCH}
                    />
                </View>
            </View>
        </View>
    )
}
