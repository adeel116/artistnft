import React from 'react';
import { View, Image } from 'react-native';
import styles from './styles';

import LOGO from 'source/assets/logo.png';
import APP_NAME from 'source/assets/app_name.png';

export default function Splash() {
    return (
        <View style={styles.container}>
            <Image
                source={LOGO}
                style={styles.logo}
            />
            <Image
                source={APP_NAME}
                style={styles.appName}
            />
        </View>
    )
}