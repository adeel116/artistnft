import React, { useRef } from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import styles from './styles';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import TransparentButton from 'source/common/button/Transparent';
import YOUTUBE from 'source/assets/youtube.png';
import INSTAGRAM from 'source/assets/insta.png';
import { useDispatch } from 'react-redux';
import InstagramLogin from 'react-native-instagram-login';

export default function CreateAccount({ navigation }) {
    const instagramLogin = useRef(null)
    const dispatch = useDispatch()

    const setIgToken = (data) => {
        console.log("Token: ", data)
    }

    return (
        <ScrollView contentContainerStyle={{ height: '100%' }}>
            <View style={[styles.container, { height: '100%' }]}>
                
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />

                <Text style={styles.heading1}>Fill in your account details using</Text>

                <TransparentButton
                    icon={YOUTUBE}
                    style={{ marginTop: 35 }}
                    onPress={()=>{}}
                    title='Login with Youtube (Google-Required)'
                />

                <TransparentButton
                    icon={INSTAGRAM}
                    style={{ marginTop: 20 }}
                    title='Login with Instagram'
                    onPress={()=>instagramLogin.current.show()}
                />

                <InstagramLogin
                    ref={instagramLogin}
                    appId='154789513242329'
                    appSecret='bac2d02c1f65b97a72c29a02a449649a'
                    redirectUrl='https://www.google.com/'
                    scopes={['user_profile', 'user_media']}
                    onLoginSuccess={setIgToken}
                    onLoginFailure={(data) => console.log(data)}
                />

                <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                    <Text style={styles.heading2}>I'dont have an account</Text>
                    <Button
                        title="NEXT"
                        onPress={()=>navigation.navigate('FanCreateAccount')}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}