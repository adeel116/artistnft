import React from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import styles from './styles';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import TransparentButton from 'source/common/button/Transparent';
import YOUTUBE from 'source/assets/youtube.png';
import { loginWithYoutube } from 'source/redux/actions/auth';
import { useDispatch } from 'react-redux';

export default function CreateAccount({ navigation }) {
    const dispatch = useDispatch()

    return (
        <ScrollView contentContainerStyle={{ height: '100%' }}>
            <View style={[styles.container, { height: '100%' }]}>
                
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />

                <Text style={styles.heading1}>Fill in your account details using</Text>

                <TransparentButton
                    icon={YOUTUBE}
                    style={{ marginTop: 35 }}
                    onPress={()=>dispatch(loginWithYoutube())}
                    title='Login with Youtube (Google-Required)'
                />

                {/*<TransparentButton
                    icon={BAND_CAMP}
                    style={{ marginTop: 20 }}
                    onPress={()=>alert('Login with Bandcamp')}
                    title='Login with Bandcamp'
                /> */}

                <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                    <Text style={styles.heading2}>I'dont have an account</Text>
                    <Button
                        title="NEXT"
                        onPress={()=>navigation.navigate('FanCreateAccount')}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}