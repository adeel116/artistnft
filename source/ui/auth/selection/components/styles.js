import { StyleSheet } from 'react-native';
import theme from 'source/constants/colors';

export default StyleSheet.create({
    container: {
        width: 18,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dot: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: theme.grey
    },
    activeDot: {
        backgroundColor: theme.dots
    }
})