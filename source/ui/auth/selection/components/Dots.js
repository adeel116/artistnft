import React from 'react'
import { View, Text } from 'react-native'
import styles from './styles'

export default function Dots({ active }) {
    return (
        <View style={styles.container}>
            <View style={[styles.dot, active === 0 ? styles.activeDot : {}]} />
            <View style={[styles.dot, active === 1 ? styles.activeDot : {}]} />
        </View>
    )
}
