import React from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import styles from './styles';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import PhoneInput from 'source/common/phoneInput';
import OTP from 'source/assets/otp.png';

export default function Selection({ navigation }) {
    return (
        <ScrollView contentContainerStyle={styles.scrollview}>
            <View style={styles.container}>
                
                <Image
                    source={APP_NAME}
                    style={styles.appName}
                />
                
                <Image
                    source={OTP}
                    style={styles.otpImage}
                />

                <Text style={styles.heading1}>Phone Verification</Text>

                <PhoneInput
                    style={{ marginTop: 35 }}
                    onPress={()=>{}}
                    title='OTP'
                />

                <Text style={[styles.heading2, styles.heading4, styles.dEnd]}>
                    <Text style={styles.heading3}>You did’nt recieve a code? </Text>
                    Re-send
                </Text>

                <View style={{ marginTop: 'auto', width: '100%', paddingBottom: 15 }}>
                    <Button
                        title="VERIFY"
                        onPress={()=>navigation.navigate('OTPVerified')}
                        style={{ marginLeft: 'auto', marginRight: 'auto' }}
                    />
                </View>
            </View>
        </ScrollView>
    )
}