import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import styles from './styles';
import CallDetectorManager from 'react-native-call-detection';

import APP_NAME from 'source/assets/app_name.png';
import Button from 'source/common/button';
import PhoneInput from 'source/common/phoneInput';
import {useDispatch} from 'react-redux';
import {verifyNumberAction} from 'source/redux/actions/reloginMCall';
// import CallDetectorManager from 'react-native-call-detection'

export default function Selection({navigation}) {
  const [country, setCountry] = useState('');
  const dispatch = useDispatch();
  const [phone, setPhone] = useState('');

  useEffect(() => {
    startListener();
  }, []);

  const startListener = () => {
    var callDetector = new CallDetectorManager(
      (event, phoneNumber) => {
        console.log(`event ${event} ${phoneNumber}`);
        alert(`event ${event} number: ${phoneNumber}`);
        // For iOS event will be either "Connected",
        // "Disconnected","Dialing" and "Incoming"

        // For Android event will be either "Offhook",
        // "Disconnected", "Incoming" or "Missed"
        // phoneNumber should store caller/called number

        if (event === 'Disconnected') {
          // Do something call got disconnected
        } else if (event === 'Connected') {
          // Do something call got connected
          // This clause will only be executed for iOS
        } else if (event === 'Incoming') {
          // Do something call got incoming
        } else if (event === 'Dialing') {
          // Do something call got dialing
          // This clause will only be executed for iOS
        } else if (event === 'Offhook') {
          //Device call state: Off-hook.
          // At least one call exists that is dialing,
          // active, or on hold,
          // and no calls are ringing or waiting.
          // This clause will only be executed for Android
        } else if (event === 'Missed') {
          // Do something call got missed
          // This clause will only be executed for Android
          dispatch({type: 'MISSED_CALL', payload: phoneNumber});
        }
      },
      true, // if you want to read the phone number of the incoming call [ANDROID], otherwise false
      () => {}, // callback if your permission got denied [ANDROID] [only if you want to read incoming number] default: console.error
      {
        title: 'Phone State Permission',
        message:
          'This app needs access to your phone state in order to react and/or to adapt to incoming calls.',
      }, // a custom permission request message to explain to your user, why you need the permission [recommended] - this is the default one
    );
  };

  const stopListenerTapped = () => {
    this.callDetector && this.callDetector.dispose();
  };

  useEffect(() => {
    requestReadPhoneStatePermission();
  }, []);

  const onVerify = () => {
    console.log('onVerify', Platform.OS);
    if (phone) dispatch(verifyNumberAction(phone, Platform.OS));
  };

  const requestReadPhoneStatePermission = async () => {
    // try {
    //   const granted = await PermissionsAndroid.requestMultiple(
    //     [
    //         PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
    //         PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE
    //     ],
    //     {
    //       title: "Cool Photo App Camera Permission",
    //       message:
    //         "Cool Photo App needs access to your camera " +
    //         "so you can take awesome pictures.",
    //       buttonNeutral: "Ask Me Later",
    //       buttonNegative: "Cancel",
    //       buttonPositive: "OK"
    //     }
    //   );
    //   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //     console.log("You can use the camera");
    //   } else {
    //     console.log("Camera permission denied");
    //   }
    // } catch (err) {
    //   console.warn(err);
    // }

    try {
      const permissions = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
        PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
      ]);
      console.log('Permissions are: ', permissions);
    } catch (err) {
      console.warn(err);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollview}>
      <View style={styles.container}>
        <Image source={APP_NAME} style={styles.appName} />

        <Text style={styles.heading1}>Welcome Back!</Text>
        <Text style={styles.heading2}>Fill in your account using</Text>

        <PhoneInput
          value={phone}
          style={{marginTop: 35}}
          title="Mobile Number"
          onChangeText={setPhone}
          setMyCountryCode={setCountry}
        />

        <Text style={[styles.heading2, styles.dEnd]}>Change Number?</Text>

        <View style={{marginTop: 'auto', width: '100%', paddingBottom: 15}}>
          <Button
            title="VERIFY"
            onPress={onVerify}
            style={{marginLeft: 'auto', marginRight: 'auto'}}
          />
        </View>
      </View>
    </ScrollView>
  );
}
