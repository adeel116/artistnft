import React from 'react'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import styles from './styles'

export default function Button({ title, onPress, style, titleStyle }) {
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={[styles.container, style]}>
                <Text style={[styles.title, titleStyle]}>{title}</Text>
            </View>
        </TouchableWithoutFeedback>
    )
}
