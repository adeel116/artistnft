import React, { useState } from 'react'
import { View, TextInput, Text, TouchableWithoutFeedback } from 'react-native'
import styles from './styles'
import CountryPicker from 'react-native-country-picker-modal'

export default function PhoneInput({ title, value, onChangeText, onPress, style, setMyCountryCode }) {
    const [countryFlag, setCountryFlag] = useState('flag-us')
    const [countryCode, setCountryCode] = useState('1')
    const [visible, setVisible] = useState(false)
    
    const onSelect = (country) => {
        setMyCountryCode(country.callingCode.length ? country.callingCode[0] : '')
        setCountryCode(country.callingCode.length ? country.callingCode[0] : '')
        setCountryFlag(country.flag)
    }

    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={[styles.transparentContainer, style]}>
                <TextInput
                    value={value || ''}
                    keyboardType="numeric"
                    placeholder={title || 'Mobile Number'}
                    style={styles.inputField}
                    placeholderTextColor="rgba(255, 255, 255, 0.3)"
                    onChangeText={onChangeText ? onChangeText : ()=>{}}
                />
                
                <TouchableWithoutFeedback onPress={()=>setVisible(true)}>
                    <View style={styles.codeView}>
                            {   countryFlag !== '' ?
                                    <Text style={styles.countryCode}>+{countryCode}</Text>
                                :
                                    null    
                            }

                            <CountryPicker
                                {...{
                                withFilter: true,
                                withFlag: true,
                                withCallingCode: true,
                                onSelect,
                                }}
                                visible={visible}
                                placeholder=""
                                onClose={()=>setVisible(false)}
                            />
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    )
}