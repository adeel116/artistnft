import { StyleSheet } from 'react-native';
import theme from 'source/constants/colors';

export default StyleSheet.create({
    container: {
        width: 20,
        height: 20,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",

        elevation: 2,
    },
    img: {
        width: 10,
        height: 10,
        resizeMode: 'contain'
    },
    unchecked: {
        backgroundColor: "#C6C6C6"
    }
});