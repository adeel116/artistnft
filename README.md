# ArtistNFT

## Description: It is an NFT based application which aconnects artists with their fans.

## Installation:

- git clone <repo-url>
- npm install
- npx react-native run-android

## Contribution of work for Sprint#1:

Since the work is divided into Sprint based ecosystem. Currently in Sprint#1 there are around 14 screens that are completed and functioning properly. You can test those screens by following the user stories present on project google sheets doc.

The following functionality is implemented so far :

- **Screen#1** : User can select between signup or login then the selected option will immediately be saved in redux store under the **authReducer** state.

- **Screen#2** : User can select between fan or artist account type then the selected option will immediately be saved in redux store under the **authReducer** state.

- **Screen#3** : At screen 3 we have to select any one social account and then it redirect to screen 4 and also token save in **authReducer**

- **Screen#4** : Here we an option to select an acccount or skip a account with next button and also saved token in SocialAccounts array at **authAction**

- **Screen#4** : Here we an option to select an acccount or skip a account with next button

- **Screen#5** : its phone verification screen here we can verify and re-send code

- **Screen#5b** : After verification it shows verify

- **Screen#6** : its basic signup form and fill feilds from social logins

- **Screen#7** : If user was select fan account at Screen#2 so he can signup from fan with basic feilds and save data on it **authReducer**

- **Screen#78** : If user was select artist account at Screen#2 so he can signup from artist with basic feilds and save data on it **userReducer**

- **Screen#9A** : Its profile detail screen for fan

- **Screen#9B** : Its profile detail screen for artist
